package S1;

public class Telefon {

    private String model;
    private long price;
    private int PIN;

    public String getModel(){
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public int getPIN() {
        return PIN;
    }

    public void setPIN(int PIN) {
        this.PIN = PIN;
    }

    private ModulGPS modulGPS;
    private Tasta tasta;
    Display di;
    Baterie b;
}
