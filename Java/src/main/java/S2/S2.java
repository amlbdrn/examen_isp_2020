package S2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.*;

public class S2 extends JFrame{
    private JButton button;
    private JTextField text1;
    private JTextField text2;
    private JLabel text;

    public S2(){
        setTitle("Display file content");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(600, 500);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        text = new JLabel("Enter file name :");
        text.setBounds(100, 100, 200, 20);

        text1 = new JTextField();
        text1.setBounds(200, 100, 200, 20);

        button = new JButton("Display file content");
        button.setBounds(180, 200, 200, 50);

        text2 = new JTextField();
        text2.setBounds(40, 300, 500, 100);


        button.addActionListener(new TratareButon());

        add(text1);
        add(text);
        add(button);
        add(text2);

    }

    class TratareButon implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String file = S2.this.text1.getText();
            try {
                BufferedReader bf = new BufferedReader(new FileReader(new File(file)));
                String l = "";
                String txt="";
                StringBuilder content = new StringBuilder();
                l = bf.readLine();
                while (l != null) {
                    txt=txt + " " + l + "\n";
                    content.append(l);
                    content.append("\r\n");
                    l = bf.readLine();
                }
                text2.setText(content.toString());

            } catch (Exception ex) {

            }
        }

    }

    public static void main(String[] args) {
        S2 fc=new S2();
    }
}

